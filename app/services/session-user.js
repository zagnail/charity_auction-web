import Ember from 'ember';

const { service } = Ember.inject;

export default Ember.Service.extend({
  session: service('session'),
  store: service(),

  setUser: Ember.computed('session.data.authenticated.access_token', function () {
    this.get('store').findRecord('user', 'me')
  })
});
